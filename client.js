var PORT = 7789;
var HOST = '52.66.255.113';

var dgram = require('dgram');
var message = new Buffer.from('My Exxar is Good!');

var client = dgram.createSocket('udp4');
client.send(message, 0, message.length, PORT, HOST, function(err, bytes) {
  if (err) throw err;
  console.log('UDP message sent to ' + HOST +':'+ PORT);
  client.close();
});
